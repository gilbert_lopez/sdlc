package org.lopez.jms.adapter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.stereotype.Component;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

@Component
public class ConsumerAdapter {
	
	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());

	public void sendToMongo(String json) {
		logger.info("In sendToMongo");
		MongoClient client = new MongoClient();
		/*DB db = client.getDB("vendor");
		DBCollection collection = db.getCollection("contact");
		logger.info("Convert JSON to DB object");
		DBObject dbObject = (DBObject)JSON.parse(json);
		collection.insert(dbObject);*/
		MongoDatabase database = client.getDatabase("vendor");
		MongoCollection<Document> collection = database.getCollection("contact");
		logger.info("Convert JSON to BSON document");
		Document document = Document.parse(json);
		collection.insertOne(document);
		client.close();
		logger.info("Sent to Mongod");
	}

}

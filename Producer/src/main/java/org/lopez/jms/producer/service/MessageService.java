package org.lopez.jms.producer.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lopez.jms.producer.model.Vendor;
import org.lopez.jms.producer.sender.MessageSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

@Component
public class MessageService {
	
	private static Logger logger = LogManager.getLogger(MessageService.class.getName());
	
	@Autowired
	MessageSender messageSender;

	public void process(Vendor vendor) {
		
		Gson gson = new Gson();
		String json = gson.toJson(vendor);
		logger.info("Message: "+json);
		messageSender.send(json);
		
	}

	
}

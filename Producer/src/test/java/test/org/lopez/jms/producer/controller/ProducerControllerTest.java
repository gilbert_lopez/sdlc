package test.org.lopez.jms.producer.controller;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.lopez.jms.producer.controller.ProducerController;
import org.lopez.jms.producer.model.Vendor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

public class ProducerControllerTest {
	
	private Vendor vendor;
	private Model model;
	private ProducerController producerController;
	private ApplicationContext context;
	

	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("spring/application-config.xml");
		producerController = (ProducerController) context.getBean("producerController");
		//producerController = new ProducerController();
		vendor = new Vendor();
		vendor.setVendorName("Google");
		vendor.setFirstName("Bob");
		vendor.setLastName("Barker");
		vendor.setAddress("100 Main Street");
		vendor.setCity("Santa Monica");
		vendor.setState("CA");
		vendor.setZipCode("909036");
		vendor.setEmail("bob.barker@gmail.com");
		vendor.setPhone("213-359-3012");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRenderVendorPage() {
		assertEquals("index", producerController.renderVendorPage(vendor, model));
	}

	@Test
	public void testProcessRequest() {
		ModelAndView mv = producerController.processRequest(vendor, model);
		assertEquals("index", mv.getViewName());
	}

}
